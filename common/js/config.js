export default {
	// 服务器请求相关配置
	SERVER:{
		// 服务器地址
		// HOST_URL: "https://demo.jeesite.com/js/",
		HOST_URL: "http://192.168.0.40:8980/js/",
		// 默认请求方式
		METHOD: "POST",
		// 默认header
		HEADER:{
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		LOADING: "加载中",
		// 默认超时时间
		TIMEOUT: 60000,
		// 数据类型
		DATA_TYPE: "json",
		// 相应数据类型
		RESPONSE_TYPE: "text",
		// 验证ssl证书
		SSL_VERIFY: true,
		// 跨域请求时是否携带凭证
		WITH_CREDENTIALS: false,
		// DNS解析时优先使用IPV4
		FIRST_IPV4: false
	},
	// 登录提交信息安全Key 加密方式：DES
	SECRET_KEY: "thinkgem,jeesite,com",
	// 默认语言
	LANGUAGE: "zh",
	// 主题
	THEME: "#39b54a",
	// 缓存
	CATCH:{
		// 用户登录信息
		USER_LOGIN_INFO: "user_login_info"
	}
}