export const SET_THEME = 'setTheme'

export const SET_SESSION_ID = 'setSessionId'

export const SET_HAS_LOGIN = "setHasLogin"

export const SET_USER = "setUser"

export const SET_LOGIN_PAGE = "setLoginPage"

export const SET_LOGIN_PAGE_VALID_CODE = "setLoginPageValidCode"

export const SET_LOGIN_PAGE_NEED_VALID = "setLoginPageNeedValid"

export const SET_LOGIN_PAGE_SID = "setLoginPageSid"

export const SET_LOGIN_PAGE_INPUT = "setLoginPageInput"

export const SET_LOGIN_PAGE_VALID_CODE_URL = "setLoginPageValidCodeUrl"


export const SET_ROLES = "setRoles"

export const SET_PERMISS = "setPermiss"