import Config from '../common/js/config'

const state = {
    HOST_URL: Config.SERVER.HOST_URL,
	theme: Config.THEME,
	secretKey: Config.SECRET_KEY,
	__sid: "",
	hasLogin: true,
	loginPage: {
		needValid: false,
		input:{
			username: "",
			password: "",
			validCode: ""
		},
		validCodeUrl: "",
		sid: ""
	},
	user:{},
	roles: [],
	stringPermissions:[]
}
export default state